#include <stdio.h>
#include <gsl/gsl_linalg.h>
#include <vectorf.h>

int main(void)
{
	gsl_matrix *A = gsl_matrix_alloc(3, 3);
	gsl_matrix_set_identity(A);
	gsl_matrix_set(A, 0, 0, 2);

	printf("A\n");
	gsl_matrix_fprintf(stdout, A, "%g");

	gsl_matrix *V = gsl_matrix_alloc(3, 3);
	gsl_vector *S = gsl_vector_alloc(3), 
				 *temp = gsl_vector_alloc(3);

	gsl_linalg_SV_decomp(A, V, S, temp);

	printf("U\n");
	gsl_matrix_fprintf(stdout, A, "%g");
	printf("S\n");
	gsl_vector_fprintf(stdout, S, "%g");
	printf("V\n");
	gsl_matrix_fprintf(stdout, V, "%g");

	mat3f B = {{1,2,3},{4,5,6},{7,8,9}};
	gsl_matrix_float_view Bv = gsl_matrix_float_view_array(&B[0][0], 3, 3);

	printf("B\n");
	gsl_matrix_float_fprintf(stdout, &Bv.matrix, "%g");

	gsl_matrix_free(A);
}