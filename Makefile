CC 			= gcc
CFLAGS 	= -Wall -g -I ../clib -D HAVE_INLINE 
LFLAGS 		= -lgsl -lopenblas -lm \
						-L/home/rgill/GIT/clib -Wl,-rpath=/home/rgill/GIT/clib -lmyclib

EX_OBJ  := $(patsubst %.c,%.out,$(wildcard ex*/*.c))

examples: $(EX_OBJ)

%.out: %.c
	$(CC) $(CFLAGS) -o $@ $< $(LFLAGS)

clean:
	rm -f $(EX_OBJ)